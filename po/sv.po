# Just Perfection GNOME Shell Extension Translation
# Copyright (C) 2020
# This file is distributed under GPL v3
# Just Perfection <justperfection.channel@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-08 07:33-0800\n"
"PO-Revision-Date: 2020-12-18 04:45-0800\n"
"Last-Translator: Just Perfection <justperfection.channel@gmail.com>\n"
"Language-Team: Swedish\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ui/prefs.ui:73
msgid "Support"
msgstr "Stöd"

#: ui/prefs.ui:87
msgid ""
"Your support helps the development process continue and brings more features "
"to the extension"
msgstr ""
"Ditt stöd hjälper utvecklingsprocessen att fortsätta och ger fler funktioner "
"till tillägget"

#: ui/prefs.ui:298
msgid ""
"You can support my work via Patreon. Also your name will be mentioned at the "
"end of videos on my YouTube Channel as Patreon supporter."
msgstr ""
"Du kan stödja mitt arbete via Patreon. Ditt namn kommer också att nämnas i "
"slutet av videor på min YouTube-kanal som Patreon-supporter."

#: ui/prefs.ui:473
msgid "Override"
msgstr "Åsidosätta"

#: ui/prefs.ui:530
msgid "Shell Theme"
msgstr "Shell-tema"

#: ui/prefs.ui:541
msgid "Overrides the shell theme partially to create a minimal desktop"
msgstr "Åsidosätter skaltema delvis för att skapa ett minimalt skrivbord"

#: ui/prefs.ui:591
msgid "Visibility"
msgstr "Synlighet"

#: ui/prefs.ui:640
msgid "Panel"
msgstr "Panel"

#: ui/prefs.ui:689
msgid "Activities Button"
msgstr "Knappen Aktiviteter"

#: ui/prefs.ui:738
msgid "App Menu"
msgstr "app-menyn"

#: ui/prefs.ui:787
msgid "Clock Menu"
msgstr "klockmeny"

#: ui/prefs.ui:836
msgid "Keyboard Layout"
msgstr "Tangentbordslayout"

#: ui/prefs.ui:885
msgid "Accessibility Menu"
msgstr "Tillgänglighetsmeny"

#: ui/prefs.ui:934
msgid "System Menu (Aggregate Menu)"
msgstr "systemmenyn"

#: ui/prefs.ui:983
msgid "Power Icon"
msgstr "Значок питания"

#: ui/prefs.ui:1032
msgid "Panel Arrow"
msgstr "panelpil"

#: ui/prefs.ui:1081
msgid "Hot Corner"
msgstr "Hot Corner"

#: ui/prefs.ui:1130
msgid "Search"
msgstr "Sök"

#: ui/prefs.ui:1179
msgid "Dash"
msgstr "Rusa"

#: ui/prefs.ui:1228
msgid "On Screen Display (OSD)"
msgstr "OSD"

#: ui/prefs.ui:1277
msgid "Workspace Popup"
msgstr "Arbetsytan Pop up"

#: ui/prefs.ui:1326
msgid "Workspace Switcher"
msgstr "Arbetsytaswitcher"

#: ui/prefs.ui:1375
msgid "Background Menu"
msgstr "Bakgrundsmeny"

#: ui/prefs.ui:1424
msgid "App Gesture"
msgstr "App gest"

#: ui/prefs.ui:1473
msgid "Window Picker Icon"
msgstr "Ikon för fönsterplockare"

#: ui/prefs.ui:1514
msgid "Behavior"
msgstr "Beteende"

#: ui/prefs.ui:1569
msgid "Type to Search"
msgstr "Skriv för att söka"

#: ui/prefs.ui:1580
msgid ""
"You can start search without search entry or even focusing on it in overview"
msgstr "Du kan börja söka utan sökpost eller ens fokusera på den i översikt"

#: ui/prefs.ui:1630
msgid "Customize"
msgstr "Anpassa"

#: ui/prefs.ui:1679
msgid "Panel Corner Size"
msgstr "Panelhörnstorlek"

#: ui/prefs.ui:1690
msgid "By Shell Theme"
msgstr "Av Shell Theme"

#: ui/prefs.ui:1691
msgid "No Corner"
msgstr "Inget hörn"

#: ui/prefs.ui:1791
msgid "Panel Position"
msgstr "Panelposition"

#: ui/prefs.ui:1802
msgid "Top"
msgstr "Topp"

#: ui/prefs.ui:1803
msgid "Bottom"
msgstr "Botten"

#: ui/prefs.ui:1843
msgid "Workspace Switcher Size"
msgstr "Arbetsytans omkopplarstorlek"

#: ui/prefs.ui:1854
msgid "Default"
msgstr "Standard"

#: ui/prefs.ui:1919
msgid "What Is Done In Love Is Well Done!"
msgstr "Vad som görs i kärlek är väl gjort!"

#: ui/prefs.ui:1930
msgid "Vincent van Gogh"
msgstr "Vincent van Gogh"

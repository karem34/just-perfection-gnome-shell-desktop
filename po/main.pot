# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Just Perfection
# This file is distributed under the same license as the Just Perfection package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Just Perfection 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-08 07:38-0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ui/prefs.ui:73
msgid "Support"
msgstr ""

#: ui/prefs.ui:87
msgid ""
"Your support helps the development process continue and brings more features "
"to the extension"
msgstr ""

#: ui/prefs.ui:298
msgid ""
"You can support my work via Patreon. Also your name will be mentioned at the "
"end of videos on my YouTube Channel as Patreon supporter."
msgstr ""

#: ui/prefs.ui:473
msgid "Override"
msgstr ""

#: ui/prefs.ui:530
msgid "Shell Theme"
msgstr ""

#: ui/prefs.ui:541
msgid "Overrides the shell theme partially to create a minimal desktop"
msgstr ""

#: ui/prefs.ui:591
msgid "Visibility"
msgstr ""

#: ui/prefs.ui:640
msgid "Panel"
msgstr ""

#: ui/prefs.ui:689
msgid "Activities Button"
msgstr ""

#: ui/prefs.ui:738
msgid "App Menu"
msgstr ""

#: ui/prefs.ui:787
msgid "Clock Menu"
msgstr ""

#: ui/prefs.ui:836
msgid "Keyboard Layout"
msgstr ""

#: ui/prefs.ui:885
msgid "Accessibility Menu"
msgstr ""

#: ui/prefs.ui:934
msgid "System Menu (Aggregate Menu)"
msgstr ""

#: ui/prefs.ui:983
msgid "Power Icon"
msgstr ""

#: ui/prefs.ui:1032
msgid "Panel Arrow"
msgstr ""

#: ui/prefs.ui:1081
msgid "Hot Corner"
msgstr ""

#: ui/prefs.ui:1130
msgid "Search"
msgstr ""

#: ui/prefs.ui:1179
msgid "Dash"
msgstr ""

#: ui/prefs.ui:1228
msgid "On Screen Display (OSD)"
msgstr ""

#: ui/prefs.ui:1277
msgid "Workspace Popup"
msgstr ""

#: ui/prefs.ui:1326
msgid "Workspace Switcher"
msgstr ""

#: ui/prefs.ui:1375
msgid "Background Menu"
msgstr ""

#: ui/prefs.ui:1424
msgid "App Gesture"
msgstr ""

#: ui/prefs.ui:1473
msgid "Window Picker Icon"
msgstr ""

#: ui/prefs.ui:1514
msgid "Behavior"
msgstr ""

#: ui/prefs.ui:1569
msgid "Type to Search"
msgstr ""

#: ui/prefs.ui:1580
msgid ""
"You can start search without search entry or even focusing on it in overview"
msgstr ""

#: ui/prefs.ui:1630
msgid "Customize"
msgstr ""

#: ui/prefs.ui:1679
msgid "Panel Corner Size"
msgstr ""

#: ui/prefs.ui:1690
msgid "By Shell Theme"
msgstr ""

#: ui/prefs.ui:1691
msgid "No Corner"
msgstr ""

#: ui/prefs.ui:1791
msgid "Panel Position"
msgstr ""

#: ui/prefs.ui:1802
msgid "Top"
msgstr ""

#: ui/prefs.ui:1803
msgid "Bottom"
msgstr ""

#: ui/prefs.ui:1843
msgid "Workspace Switcher Size"
msgstr ""

#: ui/prefs.ui:1854
msgid "Default"
msgstr ""

#: ui/prefs.ui:1919
msgid "What Is Done In Love Is Well Done!"
msgstr ""

#: ui/prefs.ui:1930
msgid "Vincent van Gogh"
msgstr ""
